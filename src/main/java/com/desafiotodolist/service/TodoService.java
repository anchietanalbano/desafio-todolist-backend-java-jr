package com.desafiotodolist.service;

import com.desafiotodolist.entity.Todo;
import com.desafiotodolist.repository.TodoRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {
    private TodoRepository tr;

    public TodoService(TodoRepository tr) {
        this.tr = tr;
    }

    public List<Todo> create(Todo todo){
        tr.save(todo);
        return list();
    }

    // DONT REPEAT YOURRSELF
    public List<Todo> list(){
        // ordena lista organizada por prioridade de forma descendente, seguida de nome de forma ascendente
        Sort sort = Sort.by("prioridade").descending().and(Sort.by("nome").ascending());
        return tr.findAll(sort);
    }
    public List<Todo> update(Todo todo){
        tr.save(todo);
        return list();
    }
    public List<Todo> delete(Long id){
        tr.deleteById(id);
        return list();
    }
}
